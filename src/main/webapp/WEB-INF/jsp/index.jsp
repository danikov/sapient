<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head lang="en">
    <title>Credit Card Manager</title>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="/main.css"/>
</head>
<body>

<div id="react"></div>

<script src="built/bundle.js"></script>

</body>
</html>
