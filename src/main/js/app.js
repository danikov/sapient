const React = require('react');
const ReactDOM = require('react-dom');

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {cards: []};
    }

    componentDidMount() {
        client({method: 'GET', path: '/api/v1/cc'}).done(response => {
            this.setState({cards: response.entity.cards});
        });
    }

    render() {
        return (
            <CardList cards={this.state.cards}/>
        )
    }
}

class CardList extends React.Component {
    render() {
        var cards = this.props.cards.map(card =>
            <Card key={card._links.self.href} card={card}/>
        );
        return (
            <table>
                <tbody>
                <tr>
                    <th>Number</th>
                    <th>Name</th>
                    <th>Balance</th>
                </tr>
                {cards}
                </tbody>
            </table>
        )
    }
}

class Card extends React.Component {
    render() {
        return (
            <tr>
                <td>{this.props.card.number}</td>
                <td>{this.props.card.name}</td>
                <td>{this.props.card.balance}</td>
            </tr>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('react')
)