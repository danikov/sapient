package com.sapient.ccmgr;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@WebListener
public class InitListener implements ServletContextListener {
    private static final Logger log = LogManager.getLogger(InitListener.class);

    private SessionFactory sessionFactory;
    private Session session = null;
    private CreditCardRepository creditCardRepo = null;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("On start web app");

        sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
        creditCardRepo = new CreditCardRepository(session);

        CreditCard newCard = new CreditCard();
        newCard.setNumber("12345657893");
        newCard.setName("Joe Bloggs");
        newCard.setBalance(BigInteger.ZERO);

        creditCardRepo.save(newCard);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("On shutdown web app");
    }
}
