package com.sapient.ccmgr;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueObjectException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import java.util.Collection;
import java.util.List;

public class CreditCardRepository implements AutoCloseable {
    private static final Logger log = LogManager.getLogger(CreditCardRepository.class);

    private final Session session;
    private final Boolean ownsSession;

    public CreditCardRepository(Session session) {
        this.session = session;
        this.ownsSession = false;
    }

    public CreditCardRepository() {
        this.session = HibernateUtil.getSessionFactory().openSession();
        ownsSession = true;
    }

    public CreditCard getByNumber(String number) {
        return (CreditCard) session.get(CreditCard.class, number);
    }

    @SuppressWarnings("unchecked")
    public List<CreditCard> getAll() {
        return (List<CreditCard>) session.createCriteria(CreditCard.class).list();
    }

    public CreditCard save(CreditCard creditCard) {
        Transaction tx = session.beginTransaction();
        try {
            session.persist(creditCard);
            tx.commit();
            log.info("Added credit card: {}", creditCard);
        } catch (ConstraintViolationException | NonUniqueObjectException ex) {
            log.debug("Failed to save credit card, already exists");
            tx.rollback();
        }

        return getByNumber(creditCard.getNumber());
    }

    public void update(CreditCard creditCard) {
        Transaction tx = session.beginTransaction();
        try {
            session.merge(creditCard);
            tx.commit();
            log.info("Updated credit card: {}", creditCard);
        } catch (ConstraintViolationException ex) {
            log.warn("Failed to update credit card", ex);
            tx.rollback();
        }
    }

    public void save(Collection<CreditCard> creditCards) {
        creditCards.forEach(this::save);
    }

    @Override
    public void close() throws HibernateException {
        if (ownsSession) {
            session.close();
        }
    }
}
