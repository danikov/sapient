package com.sapient.ccmgr;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

@WebServlet("/v1/cc")
public class CreditCardServlet extends HttpServlet {
    private static final Logger log = LogManager.getLogger(CreditCardServlet.class);

    @Override
    public void init() throws ServletException {
        super.init();

        log.info("init started");
        log.info("init finished");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try (CreditCardRepository creditCardRepo = new CreditCardRepository()) {
            List<CreditCard> creditCards = creditCardRepo.getAll();

            Gson gson = req.getHeader("Accept").contains("application/json") ?
                    new Gson() :
                    new GsonBuilder().setPrettyPrinting().create();

            resp.getWriter().println(gson.toJson(creditCards));
        } catch (IOException | HibernateException e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        try (CreditCardRepository creditCardRepo = new CreditCardRepository()) {

            CreditCard newCreditCard = new CreditCard();
            newCreditCard.setNumber(req.getParameter("number"));

            if(!CreditCard.validateNumber(newCreditCard.getNumber())) {
                req.setAttribute("feedback", "Invalid credit card number");
                req.getRequestDispatcher("index.jsp").forward(req, resp);
                return;
            }
            newCreditCard.setName(req.getParameter("name"));
            newCreditCard.setBalance(BigInteger.ZERO);

            newCreditCard = creditCardRepo.save(newCreditCard);

            if (newCreditCard != null) {
                req.getSession().setAttribute("feedback", "Credit card created (or updated)!");
                resp.sendRedirect("index.jsp");
            } else {
                req.setAttribute("feedback", "Error creating credit card!");
                req.getRequestDispatcher("index.jsp").forward(req, resp);
            }
        } catch (IOException | HibernateException e) {
            throw new ServletException(e);
        }
    }
}
