package com.sapient.ccmgr;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Arrays;

@Entity
@DynamicUpdate
@Table(name = "CreditCard", uniqueConstraints = {@UniqueConstraint(columnNames = "NUMBER")})
@Data
public class CreditCard implements Serializable {
    private static final long serialVersionUID = -1L;

    @Id
    @Column(name = "NUMBER", unique = true, nullable = false, length = 19)
    private String number;

    @Column(name = "NAME", unique = false, nullable = false, length = 70)
    private String name;

    @Column(name = "BALANCE", unique = false, nullable = false)
    private BigInteger balance;

    public static boolean validateNumber(String number) {
        return validatePositiveNumber(number) && validateLuhn10(number);
    }

    private static boolean validatePositiveNumber(String number) {
        try {
            BigInteger value = new BigInteger(number);
            if (value.compareTo(BigInteger.ZERO) < 0) {
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }

    private static boolean validateLuhn10(String number) {
        long[] digits = number.chars()
                .mapToLong(Character::getNumericValue)
                .toArray();

        for(int i = digits.length % 2; i < digits.length; i++) {
            long doubled = digits[i] * 2;
            digits[i] = doubled > 9 ? doubled - 9 : doubled;
        }

        return Arrays.stream(digits).sum() % 10 == 0;
    }
}