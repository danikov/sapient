package com.sapient.ccmgr;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreditCardRepoTest {
    private SessionFactory sessionFactory;
    private Session session = null;
    private CreditCardRepository creditCardRepo = null;

    @Before
    public void before() {
        sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
        creditCardRepo = new CreditCardRepository(session);
    }

    @Test
    public void returnsCreditCardWithMatchingNumber() {
        CreditCard creditCard = makeCreditCard();

        assertEquals(0, creditCardRepo.getAll().size());
        session.save(creditCard);
        CreditCard loadedCreditCard = creditCardRepo.getByNumber("12345678903");
        assertNotNull(loadedCreditCard);
        assertEquals(loadedCreditCard, creditCard);

        session.delete(loadedCreditCard);
        session.flush();
    }

    @Test
    public void savesCreditCard() {
        CreditCard creditCard = makeCreditCard();

        assertEquals(0, creditCardRepo.getAll().size());
        creditCardRepo.save(creditCard);
        assertEquals(1, creditCardRepo.getAll().size());

        CreditCard loadedCreditCard = creditCardRepo.getByNumber("12345678903");
        assertNotNull(loadedCreditCard);
        assertEquals(loadedCreditCard, creditCard);

        session.delete(loadedCreditCard);
        session.flush();
    }

    private CreditCard makeCreditCard() {
        CreditCard creditCard = new CreditCard();
        creditCard.setNumber("12345678903");
        creditCard.setName("Test Credit Card");
        creditCard.setBalance(BigInteger.ZERO);
        return creditCard;
    }

    @After
    public void after() {
        session.close();
    }
}
