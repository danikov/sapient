package com.sapient.ccmgr;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreditCardServletTest {
    private SessionFactory sessionFactory;
    private Session session = null;
    private CreditCardRepository creditCardRepo = null;

    @Before
    public void before() {
        sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
        creditCardRepo = new CreditCardRepository(session);
    }

    @Test
    public void testServletInitialisesCorrectly() throws Exception {
        assertEquals(0, creditCardRepo.getAll().size());
    }

    @After
    public void after() {
        session.close();
    }
}
