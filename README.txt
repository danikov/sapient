Notes:

Due to time constraints I decided to adapt this from an existing project (been doing a lot of technical tests recently).
As a result, it is using Servlets instead of SpringBoot, but it should still be suitable for a quick, thrown together app.
Only caveat is that it needs Tomcat to be deployed to unlike a SpringBoot app being able to run itself.

In terms of progress, the backend is in good shape, some invalidation is in place, and I was in the middle of debugging
the frontend when I ran out of time. I don't seem to have wired up react 100% correctly yet so it doesn't really display
anything when the page renders.

There are some very basic unit tests, but nothing that checks the validation yet, nor any other edge cases I'd like to
cover (e.g. the full bredth of REST responses).